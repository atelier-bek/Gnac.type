﻿Note d'intention du projet 
Comment sensibiliser des gens qui ne sont pas issus de la typo à ces questionnement?- Quels sont aujourd'hui nos systèmes d'écritures? (principalement numérique).

Le travail et le projet de recherche que nous présentons dans ce dossier a été initié en Décembre 2016 lors d’une séance de travail d’une semaine, dans le cadre d’une exposition présentant un état des recherches de notre Atelier, à Bruxelles. 
GNAC est né d’un premier constat : 

Aujourd’hui nous écrivons de moins en moins à la main et de plus en plus par l’intermédiaire d’outils numériques. Le geste et la singularité que l’on retrouve dans la trace écriteA tend ainsi à disparaître derrière des fontes numériques dessinées et distribuées par d’autre. 

Cette perte manifeste de maîtrise sur les intentions de l’écriture s’accompagne également d’une disparition de forme individuelles de celle-ci. Si écrire maladroitement au stylo bille n’a
pas le même dessein qu’une écriture énergique au feutre fluo, comment dès lors se joue cette translation expressive dans le paradigme d’une fonte numérique ?

Il convient ici de distinguer l’écriture que l’on définit par un geste associé à un instrument d’écriture, du lettrage qui est un dessin et de la typographie que l’on qualifiera de système de formes reproductible.

À ce changement s’associe par ailleurs une émancipation de la typographie. Si, il y a quelques dizaines d’années encore, l’accès à la création, la modification et l’usage des familles de caractères typographiques étaient restreintes, le développement des fontes numériques a changé cette donne. 
Aujourd’hui, tout un chacun dans les outils d’échanges utilise de la typographie et d’écrire un mél en LibreBaskerville ou en DéjàVu. 

L’écriture (et les outils?) sont le reflet de notre pensée. 

Comme le rappelle Katerine Hayles1, la parole, l’écriture et le code sont aujourd’hui trois grands systèmes de signification. Si l’écriture n’est pas de la parole et que le code n’est pas de l’écriture, ce qui caractérise le système de signification qu’est le code, c’est que les deux autres grands systèmes de signification -parole et écriture- passent majoritairement par lui.  


Il ne s’agit pas de remettre le geste au centre des potentiels processus et d’aboutir à un simulacre du stylo sur la feuille de papier, un simulateur de graphie, mais bien de prendre en compte l’environnement technique de l’ordinateur et de le considérer intrinsèquement comme outil d’écriture de forme. 

Revendiquer une certaine maladresse et le tatonement dans la construction et la manipulation des outils que nous mettront en place, ainsi que des formes qui en découleront nous apparaît comme une piste possible d’expressivité 

écriture geste
lettrage dessin 
typographie système de forme reproductible 

Si cette perte d’autonomie   
perte de maîtrise sur les intentions de l’écriture

il y a peu de temps peu de monde avait accès à la typo ajd tt le monde l’a mais peu on accès à la modif et à la création de ça. 

Quelles questions ça pose? 
> Des choses bien mais aussi des risques > Un monde où l'on écrit de moins en moins manuellement. > Google > mail à 3h du matin > machine et robot qui contraignent > problèmes des licences qui font que l'on ne peut plus agir sur les formes et incidemment sur nos pensées et nos intentions. écrire au marqueur n'est pas la même intention qu'écrire au stylo bille. (voir référence > Katerine Hayles parole, écriture, code) > Pas qu'une question de typographe. * Il n'y a plus de forme individuelle dans l'écriture. Il y a une perte expressivité. 





- En quoi le libre est une solution. En quoi les pratiques de l'open ls développent une certaine socialité - 

Historique et références de techniques en typo.


Qu'est-ce qu'une fonte numérique? 
- Standards en typo
qui fait l'open-type? Notamment Apple-microsoft-Adobe > Pour participer aux débats sur l'open-type il faut payer cher *

II  L'Orientation du projet

L’histoire de l’écriture est donc jalonnée par l’apparition de formes, conditionnées par des techniques qui elles même en évacuent d’autres et font émerger des standards. Ces mêmes méthodes deviendront à leur tour désuètes avec l’arrivée de nouveaux environnements techniques, les formes elles, resterons. 

 
Concrètement, il s’agirait pour nous de réactiver d

En nous mettant dans une position spéculative quant à la possibilité d’existence d’autres méthodes et standards en typographie, nous souhaitons ouvrir une question plus large liée à la notion même de langage. 
De plus cette attitude nous permet d’évacuer, du moins dans ce temps de recherche, la nécessité d’application de ces fictions typographiques. Il ne s’agit pas pour nous de faire un travail de typographe. 
Ou peut-être faut-il aborder cette question de la réappropriation expressive de l’écriture numérique tout à la fois comme un typographe -conscient d’une histoire des formes-, un designer -attentifs à la question des outils- et un artiste -dans l’exploration non conditionnée par la nécessaire fonctionnalité des formes.

La mise en place de ces méthodes et des formes qui en découleront se construiront en s’appuyant en parallèle sur un contenu éditorial. 
 
Pas des grands programmeurs/typographes mais on se donne le droit de travailler et questionner ces questions là. Repenser le langage? 
Aller sur un terrain plus spéculatif quand à l'application. (voir référence > ref. Wim Crouwel) / Prendre des cas où des personnes se sont poser des question gigantesque/ qui réinventent l'écriture.
Qu'est ce que ça veut dire de réactiver des techniques désuètes avec leur contraintes et des langages actuels de programmation.

A un moment des personnes se sont posés les bonnes questions par rapport à ces techniques mais avec l'évolution technique et la standardisation, on est très vite passer à autre chose. > Transgression des usages / Hack / Bricolage Dialogue avec l'ordinateur qu'avaient les premiers Hackers. > ordinateur comme outil reflexif. / dialogue homme-machine 

Ouverture sur la création d'un projet éditorial Ouverture et distribution
la documentation et l'ouverture par la licence est indissociable au résultat. > Lié cet aspect documentation à la démarche de Oudéis dans la documentation des résidences. *> On ne se donne pas la contrainte de faire un outil ou des choses parfaitement fonctionnelles. Plus pour s'interroger sur les méthodes et l'expérimentation. 





