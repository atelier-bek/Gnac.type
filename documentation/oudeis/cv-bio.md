Tous deux designer graphique installés à Bruxelles, nous travaillons régulièrement ensemble depuis une dizaine d'année. Après des études communes à l'Esad Valence, nous avons, avec 7 autres personnes créé l'atelier Bek en 2015 à Molenbeek-Saint-Jean. 

Baptiste Tosi :
2011-2013 Bachelor design graphique HEAD-Genève
2013-2015 DNSEP Esad-Valence
2015-... création de l'Atelier Bek 

Antoine Gelgon :

